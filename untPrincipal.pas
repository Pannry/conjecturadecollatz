unit untPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    strGrd: TStringGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    lblProblema: TLabel;
    speValue: TSpinEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure speValueChange(Sender: TObject);
  private
    { Private declarations }
    function calcCollatz(entry : Integer) : Integer;
    procedure clearRow;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.speValueChange(Sender: TObject);
var
  i, j: Integer;
  collatz : Integer;
begin
  clearRow;
  strGrd.RowCount := 1;
  i := 0;
  j := 0;

  collatz := speValue.Value;

  strGrd.Cells[i, j] := IntToStr(collatz);
  i := i + 1;

  while collatz > 1 do
  begin
    collatz := calcCollatz(collatz);
    strGrd.Cells[i, j] := IntToStr(collatz);

    i := i + 1;
    if i = 10 then
    begin
      i := 0;
      strGrd.RowCount := strGrd.RowCount + 1;
      j := j + 1;
    end;

  end;

end;

function TForm1.calcCollatz(entry: Integer): Integer;
begin
  if entry mod 2 = 0 then
  begin
    Result := entry div 2;
  end
  else
    Result := (entry * 3) + 1;
end;

procedure TForm1.clearRow;
var
  i : Integer;
begin
  for i := 0 to strGrd.ColCount - 1 do
    strGrd.Cols[i].Clear;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  lblProblema.Caption := 'Conjectura de Collatz:'+
  #10 + 'Considere a seguinte opera��o em um n�mero inteiro positivo ' +
        'arbitr�rio na qual: '+
  #10 + '* Se o n�mero � par, divida-o por 2;' +
  #10 + '* Se � �mpar, multiplique-o por 3 e some 1'
end;

end.
