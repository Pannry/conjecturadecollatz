object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Conjectura de Collatz'
  ClientHeight = 410
  ClientWidth = 656
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 656
    Height = 113
    Align = alTop
    TabOrder = 0
    object lblProblema: TLabel
      Left = 16
      Top = 9
      Width = 4
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 87
      Width = 91
      Height = 16
      Caption = 'Insira o valor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object speValue: TSpinEdit
      Left = 113
      Top = 85
      Width = 64
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 5
      OnChange = speValueChange
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 113
    Width = 656
    Height = 297
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 1
    ExplicitLeft = 72
    ExplicitTop = 111
    ExplicitWidth = 617
    ExplicitHeight = 271
    object strGrd: TStringGrid
      Left = 1
      Top = 1
      Width = 654
      Height = 295
      Align = alClient
      ColCount = 10
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      ScrollBars = ssVertical
      TabOrder = 0
      ExplicitLeft = 2
      ExplicitTop = 6
      ExplicitHeight = 303
    end
  end
end
